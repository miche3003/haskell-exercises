
module ManyMonads where

data Perhaps a = Null | Simply a deriving Show

instance Monad Perhaps where
	return = Simply
	(>>=) f g = case f of
		Null -> Null
		Simply a -> g a

instance Functor Perhaps where
	fmap f a = case a of
		Null -> Null
		Simply x -> Simply (f x)



main :: IO ()
main = return ()