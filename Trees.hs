

data Tree a = Leaf
			| Node (Tree a) a (Tree a)
			deriving Show

type TreeState = YourState

newtype YourState a = St { rS :: Int -> (a, Int) }

x = Node (Node Leaf 5 Leaf) 7 (Node Leaf 4 Leaf)

instance Monad YourState where
	-- a -> YourState a
	return x = St (\s -> (x, s) )
	-- YourState a -> (a -> YourState b) -> YourState b
	f >>= g = St $ \s -> let (a, s') = rS f s in rS (g a) s'

getSt :: YourState Int
getSt = St $ \s -> (s, s)

putSt :: Int -> YourState ()
putSt s = St $ \_ -> ((),s)

incrState :: TreeState ()
incrState =
	do
		s <- getSt -- s:: Int
		putSt $ s + 1

--           Tree a -> Int -> (Tree Int, Int)
labelTree :: Tree a -> TreeState (Tree Int)
labelTree Leaf = return Leaf
labelTree (Node l _ r) = 
	do
		s <- getSt
		incrState
		l' <- labelTree l
		r' <- labelTree r
		return (Node l' s r')

data Data = Data  deriving (Eq, Show)
