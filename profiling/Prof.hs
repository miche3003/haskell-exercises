-- PROFILING
-- http://www.cs.uu.nl/wiki/bin/viewfile/USCS/ComputerLab?rev=1;filename=Profiling.pdf
--

{-

$ ghc --make -prof -auto-all Sum1
$ ./Sum1 +RTS -K100M -p -hc
$ hp2ps Sum1.hp

-}

---- Exercise 1

rev = foldl (flip (:)) []
rev' = foldr (\x r -> r ++ [x]) []

runTest f = print $ f [1..10000]

testRev = runTest rev
testRev' = runTest rev'

---------------------------------

---- Exercise 2

conc xs ys = foldr (:) ys xs
conc' = foldl (\k x -> k . (x:)) id

test2 f = print $ f [1..100000] [1..100000]

---------------------------------

----- Exercise 3

f1 = let xs = 1:undefined:[2..100000] in if length xs > 0 then head xs else 0 
f2 = if length [1..1000000] > 0 then head [1..1000000] else 0

test3 f = print f

---------------------------------


main :: IO ()
main = test3 f1