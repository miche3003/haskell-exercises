import Graphics.SOE.Gtk
import Control.Monad.State

type Drawing = Bool

-- commands 
--		forward num
-- 		right angle
--		back num
--		left angle
--		pendown
--		penup

-- Current position, current angle, drawing or not
data TurtleState = TrSt Point Angle Drawing

data Command = Forward Int | Back Int | GoLeft Angle | GoRight Angle | Penup | Pendown

windowSize :: Size
windowSize = (600,600)

initialState :: TurtleState
initialState = TrSt (0,0) 0 False

-- processCommand :: Command -> State TurtleState Graphic
-- processCommand c = case c of
	-- Pendown -> initialState --- WRONG

-- line 
-- withColor 

main :: IO ()
main = undefined