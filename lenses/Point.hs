{-# LANGUAGE FlexibleInstances #-}

import System.IO
import Control.Lens
import Control.Monad
--import Control.Monad.Trans.State
--import qualified Data.Map as M
--import Data.Maybe

-- use lenses to move a point

data Point = Point {
	_x :: Double,
	_y :: Double
} deriving (Show)

x :: Lens' Point Double
x = lens _x (\p v-> p {_x = v})

y :: Lens' Point Double
y = lens _y (\p v -> p {_y = v})

nextPoint :: Char -> Point -> Point
nextPoint c p = case c of
	'a' -> over x subOne p
	's' -> over y subOne p
	'd' -> over x addOne p
	'w' -> over y addOne p
	_   -> p

subOne, addOne :: Num a => a -> a
subOne z = z - 1
addOne z = z + 1

loop :: Point -> IO ()
loop p = do 
	hSetBuffering stdin NoBuffering
	c <- getChar
	let p' = nextPoint c p
	putStrLn $ '\n' : show p'
	when (c /= 'q') (loop p')

initPoint :: Point
initPoint = Point { _x = 0, _y = 0 }

main :: IO ()
main = loop initPoint -- loop initPoint