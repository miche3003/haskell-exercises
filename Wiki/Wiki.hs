{-# LANGUAGE NoMonomorphismRestriction #-}

module Wiki where

--------------------------------------------------------------------------------

-- List of imported modules and functions

import Text.XHtml
import Network.Shed.Httpd
import qualified Text.ParserCombinators.UU.BasicInstances as UU
import Text.ParserCombinators.UU.Core
import Text.ParserCombinators.UU.Utils
import Text.ParserCombinators.UU.Derived
import Text.ParserCombinators.UU.Demo.Examples
import Control.Applicative
-- import Network.URI

--------------------------------------------------------------------------------

-- | String formatting alternatives for inline text.
type Words = [String]

data Inline
  = Bold Words
  | Italic Words
  | WikiLink String
  | URILink {uriLinkRef :: String, uriLinkName :: String}
  | Unformatted Words
  deriving (Eq, Show)

-- | A list of (un)formatted strings.

type Text
  = [Inline]

-- | Text formatting alternatives for blocks of text (between newlines).

data Block
  = Bullet Text
  | Paragraph Text
  deriving (Eq, Show)

-- | A list of (un)formatted blocks.

type Page
  = [Block]

{- 

  Page        := BlockList
  Block       := Block | Block '\n' BlockList
  Block       := Bullet | Paragraph | Numbered | Header
  Bullet      := '*' ' ' Text
  Paragraph   := '#' ' ' Text
  Header      :=  '=' =' ' ' Header
  Paragraph   := Text
  Text        := InlineList
  InlineList  := Inline | Inline InlineList
  Inline      := Bold | Italic | Link | WikiLink | Unformatted
  Bold        := '**' String '**'
  Italic      := '//' String '//'
  Link        := '[[' String '|' String ']]'
  WikiLink    := '[[' String ']]'
  Unformatted := String
  String      := <anychar>

-}

--------------------------------------------------------------------------------

-- | Split a string into its lexical components to simplify manipulation.

inlinify :: String -> [String]
inlinify [] = []
inlinify str =
  case split [] str of
    (ln, wd, next) -> appCons reverse ln . appCons id wd $ inlinify next
  where
    split ln ('*':'*':cs) = (ln, "**", cs)
    split ln (c:cs)       = split (c : ln) cs
    split ln ""           = (ln, "", "")

-- | Apply a function to a list and cons the result to another list.

appCons :: ([a] -> b) -> [a] -> [b] -> [b]
appCons _ [] = id
appCons f xs = (f xs :)

-- | Split lines into their lexical components to simplify manipulation.

blockify :: [String] -> [String]
blockify [] = []
blockify lns =
  case split [] lns of
    (paras, ln, next) -> appCons (unwords . reverse) paras . appCons id ln $ blockify next
  where
    split paras (l@('*':' ':_):ls)     = (paras, l, ls)
    split paras ("":ls)                = (paras, [], ls)
    split paras (l:ls)                 = split (l : paras) ls
    split paras []                     = (paras, [], [])

-- | Merge two |Maybe| values with priority on the first one.

mplus :: Maybe a -> Maybe a -> Maybe a
mplus Nothing y = y
mplus x       _ = x

--------------------------------------------------------------------------------

oneOf "" = pFail
oneOf (h:t) = UU.pSym h <|> oneOf t

pLetter' = oneOf "abcdefghijklmnoprstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
pNum    = oneOf "0123456789"

pNumLet = (pLetter' <|> pNum)

pWords = p

pWrapped f s = f <$> UU.pToken s <*> some pNumLet <*> UU.pToken s
pWrappedIn f s s' = f <$> UU.pToken s <*> some pNumLet <*> UU.pToken s'

pBold = pWrapped (\a b c -> Bold b) "**"
pItalic = pWrapped (\a b c -> Italic b ) "//"
pWikiLink = pWrappedIn (\a b c -> WikiLink b) "[[" "]]"
pLink = (\a b c d e -> URILink b d) 
  <$> UU.pToken "[[" 
  <*> some pNumLet 
  <*> UU.pSym '|'
  <*> some pNumLet 
  <*> UU.pToken "]]"

pUnformatted = Unformatted <$> some pNumLet

pInline = pBold <|> pItalic <|> pWikiLink <|> pLink <|> pUnformatted
--------------------------------------------------------------------------------

-- Ex. 5
-- | This function splits the input string dividing the
--   DSL reserved words from the rest
-- pText :: String -> Text
--readText s = let txtLst = readText' s
--  in parseInlines s
--  where parseInlines s = case s of 
--                          Just (x,y) -> undefined -- [x] ++ parseInlines y
--                          Nothing    -> []

pText = (\x y-> x) <$> pList pInline <*> pEnd

readText' :: String -> [String]
readText' input = let (x,y) = myBreak input isReserved
                  in [x] ++ readText' y

myText = "**This is bold** and this is //italic// but this is a [[Link]]"

keywords :: [Char]
keywords = ['*', '/', '[']

isReserved :: Char -> Char -> Bool
isReserved x y = elem x keywords && x == y

myBreak :: [a] -> (a -> a -> Bool) -> ([a],[a])
myBreak (x:y:xs) pr | pr x y       = ([x,y], xs)
myBreak (x:y:xs) pr | not $ pr x y = let (ys, zs) = myBreak (y:xs) pr in (x:ys,zs)
myBreak (x:[]) pr                  = ([x],[])
myBreak [] _                       = ([],[])

--------------------------------------------------------------------------------

-- Ex. 6

readBlock :: String -> Block
readBlock inp = undefined

readUnformattedBlock :: String -> Maybe Block
readUnformattedBlock = undefined --  Just . Paragraph . readText

readBullet :: String -> Maybe Block
readBullet inp =
  case inp of
    -- '*':' ':txt  -> Just . Bullet $ readText txt
    _            -> Nothing

--------------------------------------------------------------------------------

-- Ex. 7

readPage :: String -> Page
readPage = undefined

--------------------------------------------------------------------------------

-- Ex. 9

-- | Example use of Text.HTML: make a paragraph with |"Hello, World!"| in bold.

exampleHtml :: Html
exampleHtml = thehtml << body << p << bold << "Hello, World!"

inlineToHtml :: Inline -> Html
inlineToHtml = undefined

textToHtml :: Text -> Html
textToHtml = undefined

--------------------------------------------------------------------------------

-- Ex. 10

-- | Example unordered list with list items in HTML.

exampleList :: Html
exampleList = ulist << [li << "item 1", li << "item 2"]

blockToHtml :: Block -> Html
blockToHtml = undefined

pageToHtml :: Page -> Html
pageToHtml = undefined

-- | Convert a wiki-formatted string to a renderable HTML string.

wikify :: String -> String
wikify = renderHtml . (body <<) . pageToHtml . readPage

--------------------------------------------------------------------------------

simpleServer :: Request -> IO Response	
simpleServer = return . Response responseOK [contentText] . show

initMyServer :: (Request -> IO Response) -> IO Server
initMyServer = initServer myPort

myPort :: Int
myPort = 7734

contentText, contentHtml :: (String, String)
contentText = contentType "text/plain"
contentHtml = contentType "text/html"

responseOK, responseNotFound :: Int
responseOK       = 200
responseNotFound = 404

methodGET, methodPOST :: String
methodGET  = "GET"
methodPOST = "POST"

--------------------------------------------------------------------------------

-- Ex. 12

fileServer :: Request -> IO Response	
fileServer = undefined

--------------------------------------------------------------------------------

-- Ex. 13

-- | Example form with a single text field.

exampleForm :: Html
exampleForm =
  form ! [action $ "http://localhost:" ++ show myPort ++ "/post", method methodPOST]
    << [ input ! [thetype "text"]
       , input ! [thetype "submit", value "Submit"]
       ]

wikiFormServer :: Request -> IO Response	
wikiFormServer = undefined

