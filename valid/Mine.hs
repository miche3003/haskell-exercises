
toDigits :: Integer -> [Integer]
toDigits 0 = [0]
toDigits x = toDigits' x

toDigits' x | x < 0 = let x' = x * (-1) 
					 in let xs = toDigits' (div x' 10) 
					    in  xs ++ [mod x 10]
toDigits' x | x > 0 = let xs = toDigits' (div x 10) in  xs ++ [mod x 10]
toDigits' _ = []

toDigitsRev :: Integer -> [Integer]
toDigitsRev = reverse . toDigits

doubleSecond :: [Integer] -> [Integer]
doubleSecond = doubleSecond' False

doubleSecond' :: Bool -> [Integer] -> [Integer]
doubleSecond' True  (x:xs)  = (x * 2) : doubleSecond' False xs
doubleSecond' False (x:xs)  = x : doubleSecond' True xs
doubleSecond' _     []      = []

-- sumDigits :: [a] -> a
sumDigits = sum . concatMap toDigits
validate :: Integer -> Bool
validate x = 0 == mod (f x) 10
		where f = sumDigits . doubleSecond . toDigitsRev

readCC x = read (filter (/= ' ') x)::Integer

showCC :: Integer -> String
showCC x | length (toDigits x) < 16 = let zeros = repeat (0::Integer);
							   o     = 16 - length (toDigits x)
						   in showCC $ x * 10^o
showCC x = let l = concatMap show $ toDigits x
		   in take 4 l ++ showCC' 3 (drop 4 l)

showCC' x l | x > 0  = " " ++ take 4 l ++ showCC' (x-1) (drop 4 l)
showCC' x l | x == 1 = take 4 l
showCC' _ _          = []


main :: IO ()
main = undefined