import Control.Monad.Cont

type CIO r a = ContT r IO a

for :: ( Int, Int -> Bool, Int -> Int) ->
	( CIO r s -> CIO r t -> Int -> CIO r ()) -> CIO r ()
for ( i,c,s) body 
	| c i = callCC (\break -> callCC (\continue -> 
		body (break ()) (continue ()) i) >> for (s i , c, s) body)
	| otherwise = return ()

main = runContT main' return

main' :: CIO r ()
main' = for(0,const True,(+1))
	(\break continue i ->
		do when (even i) continue
		   when (i>12) break
		   lift $ putStrLn  $ "iteration " ++ show i)