{-# LANGUAGE FlexibleInstances, MultiParamTypeClasses, FunctionalDependencies, NoMonomorphismRestriction #-}

module MMS where

import Control.Monad.State
import qualified Data.Map as M

-- |This class is not defined anywhere, so we need to define it.

class MonadState s m => StoreState s m | m -> s where
  saveState :: m ()
  loadState :: m ()

-- |Our custom /MMS_Internal/ state will receive a user suplied type, that's
--  actually the state that the user wishes to maintain, but we'll also
--  add a Map to track the number of calls.
type MMS_Internal s = (M.Map String Int, [s])

-- |We need to build our internal state somehow.
--  /mmsBuildFor ["bind", "return"] s/ will build a state
--  and track calls made to bind and return.
mmsBuild :: s -> MMS_Internal s
mmsBuild x = (M.empty, [x])

-- |Increment a given key, if it's already in the map.
--  Note that this function maintains the user suplied state /s/.
mmsIncKey :: String -> MMS_Internal s -> MMS_Internal s
mmsIncKey key (map, us) = case M.lookup key map of
                            Nothing -> (M.insert key 1 map, us)
                            _       -> (M.adjust (+1) key map, us)

-- |Our MyMonadState wil store a user suplied state /s/,
--  return a user suplied type /a/ and also record the number
--  of calls we made, in our /MMS_Internal/.
newtype MMS s a = MMS { extMMS :: MMS_Internal s -> Either String (MMS_Internal s, a) }

-- |We have to declare it as a monad first, remenbering that we want to record
--  how many times we called /bind/ and /return/.
--
--  This instance can be quite tricky, /s/ denotes the type of the user state and /x/
--  denotes the user state at the value level.
instance Monad (MMS s) where
  return val = MMS $ \x -> Right (mmsIncKey "return" x, val)
  
  f >>= g    = MMS $ \x -> case extMMS f x of
                              Right (x', out) -> extMMS (g out) (mmsIncKey "bind" x')
                              Left  err       -> Left err
                              
  fail msg   = MMS $ \_ -> Left msg

-- |Note that                    
instance MonadState s (MMS s) where
  -- |get is pretty trivial, we just return the second
  --  part of the internal state (user state)
  --  Note that we will return the top of the state stack,
  --  that's why we have a /head/ there.
  get = MMS $ \x -> Right (mmsIncKey "get" x, head (snd x))
  
  -- |put is not that trivial. We have to change the user state, but maintain
  --  the internal dictionary /i/.
  --  Putting a new state will create a new state stack.
  put x = MMS $ \(i, (_:xs)) -> Right (mmsIncKey "put" (i, x:xs), ())
  
instance StoreState s (MMS s) where
  saveState = MMS $ \(i, x) -> Right ((i, (head x):x), ())
  loadState = MMS $ \(i, x) -> case x of
                                  []     -> fail "loadState on empty stack."
                                  (_:xs) -> Right ((i, xs), ())

-- |The diagnostic function only needs to return, as a String,
--  the internal map /i/, maintaining the user state /s/.
diagnostics :: MMS s String
diagnostics = MMS $ \(i, s) -> Right ((i, s), show $ M.toList i)

-- |To create a new label, the actual manipulation is like the bind operator,
--  we receive a /MMS_Internal/ and increase the given key before running /f/.
annotate :: String -> MMS s a -> MMS s a
annotate lbl f = MMS $ \x -> extMMS f (mmsIncKey lbl x) 

-- |Top-level function for running MMS computations
runMMS :: MMS s a -> s -> Either String (s, a)
runMMS f us = case extMMS f (mmsBuild us) of
                  Right ((_, us), val) -> Right (head us, val)
                  Left err             -> Left err


test :: MMS () String
test = 
  do
    return 1
    return 2
    annotate "A" (return 3 >> return 4)
    annotate "A" (return 3)
    diagnostics
    
test2 =
  do
    put 1
    i1 <- get; saveState
    modify (*2)
    i2 <- get; saveState
    modify (*2)
    i3 <- get; loadState
    i4 <- get; loadState
    i5 <- get
    return (i1, i2, i3, i4 ,i5)
    





