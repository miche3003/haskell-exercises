{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}

import Control.Monad.State
import qualified Data.Map as M

type InternalState s = ( M.Map String Int, s )

newtype ESM s a = ESM { runESM :: InternalState s -> (InternalState s, a) }

track :: String -> InternalState s -> InternalState s
track k (m,s) = case M.lookup k m of
			  Nothing -> (M.insert k 1 m, s)
			  _       -> (M.adjust (+1) k m, s)

instance Monad (ESM s) where
	-- | gets a state
	return x = ESM (\s -> (track "return" s, x))
	f >>= g  = ESM (\s -> let (x,out) = runESM f s in runESM (g out) (track "bind" x))

instance MonadState s (ESM s) where
	get = ESM (\s -> (track "get" s, snd s))
	put s = ESM (\x -> (track "put" (fst x, s), ()))

diagnostics :: ESM s String
diagnostics = ESM $ \(x,s) -> ((x,s), show $ M.toList x)


main :: IO ()
main = do
		return 5
		diagnostics
